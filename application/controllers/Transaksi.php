<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("transaksi_m");
        
        require_once APPPATH.'third_party/fpdf/fpdf.php';
        
        $pdf = new FPDF();
        $pdf->AddPage();
        
        $CI =& get_instance();
        $CI->fpdf = $pdf;

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $transaksi = $this->transaksi_m;
        $validation = $this->form_validation;
        $validation->set_rules($transaksi->rules());
        if ($validation->run()) {
            $transaksi->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Transaksi berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("transaksi");
        }
        $data["title"] = "Transaksi";

        $id_jenis_sampah = $this->input->post('id_jenis_sampah');
        $excel = $this->input->post('excel');

        if ($id_jenis_sampah == '') $id_jenis_sampah = 0;

        $data['sampah'] = $this->transaksi_m->getSampah();
        $data['jenis_sampah'] = $this->transaksi_m->getJenisSampah();
        $data['user'] = $this->transaksi_m->getUser();

        if ($this->session->userdata['level'] == '2') {
            $data["data_transaksi"] = $this->transaksi_m->getDataOne($this->session->userdata['id']);
        }else{
            $data["data_transaksi"] = $this->transaksi_m->getAll($id_jenis_sampah);
        }
        $data["id_jenis_sampah"] = $id_jenis_sampah;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('transaksi/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('transaksi/excel', $data);
    }

    public function edit($id = null)
    {
        $data['sampah'] = $this->transaksi_m->getSampah();
        $data['user'] = $this->transaksi_m->getUser();
        $this->form_validation->set_rules('id_sampah', 'id_sampah', 'required');
        $this->form_validation->set_rules('harga_jual', 'harga_jual', 'required');
        $this->form_validation->set_rules('jumlah', 'jumlah', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Transaksi gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('transaksi');
        } else {
            $id_sampah = $this->input->post('id_sampah');
            $jumlah = $this->input->post('jumlah');
            $harga_jual = $this->input->post('harga_jual');
            $sqlxx = " select id,harga_nasabah,jumlah FROM sampah WHERE id = '$id_sampah' ";
            $queryxx = $this->db->query($sqlxx);

            if ($queryxx->num_rows() > 0) {
                $hasilxx = $queryxx->row();
                
                $harga    = $hasilxx->harga_nasabah;
                $jumlah_awal    = $hasilxx->jumlah;
            }

            $jumlah_akhir = $jumlah_awal-$jumlah;

            $data = array(
                'jumlah' => $jumlah_akhir,
                'tgl_update' => date('Y-m-d H:i:s'),
                'user_update_by' => $this->session->userdata['username']
            );
            $this->db->where('id', $id_sampah);
            $this->db->update('sampah', $data);

            $total_akhir = $jumlah*$harga_jual;

            $data = array(
                "id" => $this->input->post('id'),
                "id_user" => $this->input->post('id_user'),
                "id_sampah" => $id_sampah,
                "harga_jual" => $harga_jual,
                "jumlah" => $jumlah,
                "total_harga" => $total_akhir,
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['nama']
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('transaksi', $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Transaksi berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('transaksi');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Transaksi gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('transaksi');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('transaksi');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Transaksi berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('transaksi');
        }
    }

    public function invoice($id){
        $no=1;
        $data = $this->transaksi_m->getOne($id)->row_array();
        
        $id = $data['id'];
        $tgl_input = $data['tgl_input'];
        $user_update_by = $data['user_update_by'];

        $pdf = new FPDF('P','mm','A5'); 
        $pdf->SetMargins(10 ,10, 10 );
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',13);
        $pdf->Cell(0,7,'Invoice Bank Sampah Mekar Jaya',0,1,'C');
        $pdf->Cell(0,7,'Tanggal Transaksi : '.$tgl_input,0,1,'C');
        $pdf->SetFont('Arial','B',10);
        $pdf->SetLineWidth(1);
        $pdf->SetDrawColor(1,1,1);
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(15,5,'',0,0,'L');
        $pdf->Cell(15,5,'',0,0,'L');
        $pdf->Cell(70,5,'',0,1,'L');
        $pdf->Cell(0,7,'ID Transaksi : '.$id,0,1,'L');
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(0,7,'Admin : '.$user_update_by,0,1,'L');
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(25,8,'Jenis Sampah',1,0,'C');
        $pdf->Cell(25,8,'Harga Nasabah',1,0,'C');
        $pdf->Cell(25,8,'Harga Jual',1,0,'C');
        $pdf->Cell(25,8,'Jumlah',1,0,'C');
        $pdf->Cell(25,8,'Total',1,0,'C');
        $pdf->Ln();
        $query=$this->transaksi_m->getOne($id);
        foreach ($query->result_array() as $row)
        {
        $pdf->Cell(25,8,$row['jenis_sampah'],1,0,'C');
        $pdf->Cell(25,8,$row['harga_nasabah'],1,0,'C');
        $pdf->Cell(25,8,$row['harga_jual'],1,0,'C');
        $pdf->Cell(25,8,$row['jumlah'],1,0,'C');
        $pdf->Cell(25,8,"Rp " . number_format($row['total_harga'],2,',','.'),1,0,'C');
        $pdf->Ln();
        }
        $pdf->Output();
    }  
}
