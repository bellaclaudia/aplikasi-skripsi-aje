<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sampah extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("sampah_m");
    }

    public function index()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
        $sampah = $this->sampah_m;

        $validation = $this->form_validation;
        $validation->set_rules($sampah->rules());
        if ($validation->run()) {
            $sampah->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Sampah berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("sampah");
        }
        $id_jenis_sampah = $this->input->post('id_jenis_sampah');
        $excel = $this->input->post('excel');

        if ($id_jenis_sampah == '') $id_jenis_sampah = 0;

        $data['nasabah'] = $this->sampah_m->getNasabah();
        $data['jenis_sampah'] = $this->sampah_m->getJenisSampah();
        $data["data_sampah"] = $this->sampah_m->getAll($id_jenis_sampah);

        $data["id_jenis_sampah"] = $id_jenis_sampah;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('sampah/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('sampah/excel', $data);
    }

    public function nasabah()
    {
        $sampah = $this->sampah_m;
        $validation = $this->form_validation;
        $validation->set_rules($sampah->rules());
        if ($validation->run()) {
            $sampah->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Sampah berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("home");
        }
        $this->load->view('home', $data);
    }

    public function edit($id = null)
    {
        $data['nasabah'] = $this->sampah_m->getNasabah();
        $data['jenis_sampah'] = $this->sampah_m->getJenisSampah();
        $this->form_validation->set_rules('id_nasabah', 'id_nasabah', 'required');
        $this->form_validation->set_rules('id_jenis_sampah', 'id_jenis_sampah', 'required');
        $this->form_validation->set_rules('harga_nasabah', 'harga_nasabah', 'required');
        $this->form_validation->set_rules('jumlah', 'jumlah', 'required');
        $this->form_validation->set_rules('satuan', 'satuan', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Sampah gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('sampah');
        } else {
            $data = array(
                "id" => $this->input->post('id'),
                "id_nasabah" => $this->input->post('id_nasabah'),
                "id_jenis_sampah" => $this->input->post('id_jenis_sampah'),
                "harga_nasabah" => $this->input->post('harga_nasabah'),
                "jumlah" => $this->input->post('jumlah'),
                "satuan" => $this->input->post('satuan'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('sampah', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Sampah berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('sampah');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Sampah gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('sampah');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('sampah');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Sampah berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('sampah');
        }
    }
}
