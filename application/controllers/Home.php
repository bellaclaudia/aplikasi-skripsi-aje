<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->model("dashboard_m");
    }

    public function index()
    {
        $data["title"] = "Home";
        // $this->load->view('templates/header',$data);
        // $this->load->view('templates/menu');
        $this->load->view('home',$data);
        // $this->load->view('templates/footer');
    }
}
