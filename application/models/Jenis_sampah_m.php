<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis_sampah_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'jenis_sampah';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'jenis_sampah',  //samakan dengan atribute name pada tags input
                'label' => 'Jenis Sampah',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll()
    {
        $this->db->select('a.*');
        $this->db->from('jenis_sampah a');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    //menyimpan data
    public function save()
    {
        $data = array(
            "jenis_sampah" => $this->input->post('jenis_sampah'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
