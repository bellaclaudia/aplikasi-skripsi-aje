<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi_m extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }

    private $table = 'transaksi';
  
    public function rules()
    {
        return [
            [
                'field' => 'id_user',
                'label' => 'ID User',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_sampah',
                'label' => 'ID Sampah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'harga_jual',
                'label' => 'Harga Jual',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'jumlah',
                'label' => 'Jumlah',
                'rules' => 'trim|required'
            ]
        ];
    }

    public function getAll($id_jenis_sampah)
    {
        $this->db->select('a.*, e.jenis_sampah, d.nama as nama_nasabah, d.no_hp, d.alamat');
        $this->db->from('transaksi a');
        $this->db->join('sampah c', 'a.id_sampah = c.id', 'left');
        $this->db->join('master_user d', 'd.id = a.id_user', 'left');
        $this->db->join('jenis_sampah e', 'e.id = c.id_jenis_sampah', 'left');
        $this->db->order_by("a.id", "desc");
        if ($id_jenis_sampah != '0')
            $this->db->where("c.id_jenis_sampah", $id_jenis_sampah);

        $query = $this->db->get();
        return $query->result();
    }

    public function getDataOne($id_user)
    {
        $this->db->select('a.*, e.jenis_sampah, d.nama as nama_nasabah, d.no_hp, d.alamat');
        $this->db->from('transaksi a');
        $this->db->join('sampah c', 'a.id_sampah = c.id', 'left');
        $this->db->join('master_user d', 'd.id = a.id_user', 'left');
        $this->db->join('jenis_sampah e', 'e.id = c.id_jenis_sampah', 'left');
        $this->db->order_by("a.id", "desc");
        $this->db->where("a.id_user", $this->session->userdata['id']);
        $query = $this->db->get();
        return $query->result();
    }

    public function getSampah()
    {
        $this->db->select('a.*, b.jenis_sampah');
        $this->db->from('sampah a');
        $this->db->join('jenis_sampah b', 'a.id_jenis_sampah = b.id', 'left');
        $this->db->where("a.jumlah >",0);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getUser()
    {
        return $this->db->get('master_user')->result_array();
    }

    public function getJenisSampah()
    {
        return $this->db->get('jenis_sampah')->result_array();
    }

    public function save()
    {
        $id_sampah = $this->input->post('id_sampah');
        $jumlah = $this->input->post('jumlah');
        $harga_jual = $this->input->post('harga_jual');
        $sqlxx = " select id,harga_nasabah,jumlah FROM sampah WHERE id = '$id_sampah' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $harga    = $hasilxx->harga_nasabah;
            $jumlah_awal    = $hasilxx->jumlah;
        }

        $data = array(
            'jumlah' => $jumlah_awal-$jumlah,
            'tgl_update' => date('Y-m-d H:i:s'),
            'user_update_by' => $this->session->userdata['username']
        );
        $this->db->where('id', $id_sampah);
        $this->db->update('sampah', $data);

        $data = array(
            "id_user" => $this->input->post('id_user'),
            "id_sampah" => $id_sampah,
            "harga_jual" => $harga_jual,
            "jumlah" => $jumlah,
            "total_harga" => $jumlah*$harga_jual,
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['nama']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getOne($id)
    {
        $this->db->select('a.*, e.jenis_sampah, c.harga_nasabah');
        $this->db->from('transaksi a');
        $this->db->join('sampah c', 'a.id_sampah = c.id', 'left');
        $this->db->join('jenis_sampah e', 'e.id = c.id_jenis_sampah', 'left');
        $this->db->where("a.id", $id);
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query;
    }
}