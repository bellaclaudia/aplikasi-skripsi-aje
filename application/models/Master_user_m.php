<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_user_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_user';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'level',  //samakan dengan atribute name pada tags input
                'label' => 'Level Akses',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'nama',  //samakan dengan atribute name pada tags input
                'label' => 'Nama Lengkap',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'alamat',  //samakan dengan atribute name pada tags input
                'label' => 'Alamat',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'no_hp',  //samakan dengan atribute name pada tags input
                'label' => 'No. Handphone',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'username',  //samakan dengan atribute name pada tags input
                'label' => 'username',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'userpass',
                'label' => 'Nama Prodi',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll()
    {
        $this->db->select('a.*');
        $this->db->from('master_user a');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    //menyimpan data
    public function save()
    {
        $data = array(
            "level" => $this->input->post('level'),
            "nama" => $this->input->post('nama'),
            "alamat" => $this->input->post('alamat'),
            "no_hp" => $this->input->post('no_hp'),
            "username" => $this->input->post('username'),
            "userpass" => md5($this->input->post('userpass')),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
