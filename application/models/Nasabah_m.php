<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nasabah_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'nasabah';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'nama',
                'label' => 'Nama Lengkap',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'no_hp',
                'label' => 'No. Handphone',
                'rules' => 'trim|required'
            ]
        ];
    }

    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function save()
    {
        $data = array(
            "nama" => $this->input->post('nama'),
            "alamat" => $this->input->post('alamat'),
            "no_hp" => $this->input->post('no_hp'),
            "tgl_input" => date('Y-m-d H:i:s')
        );
        return $this->db->insert($this->table, $data);
    }
}
