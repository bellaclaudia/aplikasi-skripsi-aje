<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sampah_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'sampah';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_nasabah',
                'label' => 'Nama Nasabah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_jenis_sampah',
                'label' => 'Jenis Sampah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'harga_nasabah',
                'label' => 'Harga Nasabah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'jumlah',
                'label' => 'Jumlah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'satuan',
                'label' => 'Satuan',
                'rules' => 'trim|required'
            ]
        ];
    }

    public function getAll($id_jenis_sampah)
    {
        $this->db->select('a.*, b.nama as nama_nasabah, b.no_hp, b.alamat, c.jenis_sampah');
        $this->db->from('sampah a');
        $this->db->join('nasabah b','b.id = a.id_nasabah','left');
        $this->db->join('jenis_sampah c','c.id = a.id_jenis_sampah','left');
        $this->db->order_by("a.id", "desc");
        if ($id_jenis_sampah != '0')
            $this->db->where("a.id_jenis_sampah", $id_jenis_sampah);
        if ($this->session->userdata['level'] == '2')
            $this->db->where("a.jumlah >", 0);
        $query = $this->db->get();
        return $query->result();
    }

    public function getNasabah()
    {
        return $this->db->get('nasabah')->result_array();
    }

    public function getJenisSampah()
    {
        return $this->db->get('jenis_sampah')->result_array();
    }

    public function save()
    {
        // $tgl_skrg = date('Y-m-d H:i:s');
        // $data = array(
        //     'nama' => $this->input->post('nama_lengkap'),
        //     'alamat' => $this->input->post('alamat'),
        //     'no_hp' => $this->input->post('no_hp'),
        //     'tgl_input' => $tgl_skrg
        // );

        // $this->db->insert('nasabah', $data);

        // $id_nasabah = $this->db->insert_id();

        $data = array(
            "id_nasabah" => $this->input->post('id_nasabah'),
            "id_jenis_sampah" => $this->input->post('id_jenis_sampah'),
            "harga_nasabah" => $this->input->post('harga_nasabah'),
            "jumlah" => $this->input->post('jumlah'),
            "satuan" => $this->input->post('satuan'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
