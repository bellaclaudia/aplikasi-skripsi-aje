<!DOCTYPE html>
<html>

<head>
	<title>Bank Sampah</title>

	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" content="Relief Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	<link href="<?= base_url(); ?>assets/website/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?= base_url(); ?>assets/website/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?= base_url(); ?>assets/website/css/prettyPhoto.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url(); ?>assets/website/css/easy-responsive-tabs.css" rel='stylesheet' type='text/css' />
	<link href="<?= base_url(); ?>assets/website/css/fontawesome-all.css" rel="stylesheet">
	<!-- //for bootstrap working -->
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,500,500i,600,600i,700" rel="stylesheet">
	<link href="<?= base_url(); ?>assets/vendors/select2/css/select2.min.css" rel="stylesheet">
</head>

<body>

	<div class="top_header" id="home">
		<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="nav_top_fx_w3layouts_agileits">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					    aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="logo_wthree_agile">
						<h1>
							<a class="navbar-brand" href="<?= base_url(); ?>">
								<i class="fas fa-home" aria-hidden="true"></i> Bank Sampah Mekar Jaya
								<!-- <span class="desc">Give a little. Help a lot.</span> -->
							</a>
						</h1>
					</div>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<div class="nav_right_top">
						<ul class="nav navbar-nav">
							<li class="active">
								<a href="<?= base_url(); ?>">Home</a>
							</li>
							<li>
								<a href="<?= base_url('login'); ?>">Login</a>
							</li>
						</ul>
					</div>
				</div>
				<!--/.nav-collapse -->
			</div>
		</nav>
		<div class="clearfix"></div>
	</div>
	<!-- banner -->
	<div class="banner_top">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1" class=""></li>
				<li data-target="#myCarousel" data-slide-to="2" class=""></li>
				<li data-target="#myCarousel" data-slide-to="3" class=""></li>
			</ol>
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<div class="container">
						<div class="carousel-caption">
							<h3>Selamat Datang</h3>
						</div>
					</div>
				</div>
				<div class="item item2">
					<div class="container">
						<div class="carousel-caption">
							<h3>Website Bank Sampah Mekar Jaya</h3>
						</div>
					</div>
				</div>
				<div class="item item3">
					<div class="container">
						<div class="carousel-caption">
							<h3>Pengelolaan sampah menjadi mudah</h3>
						</div>
					</div>
				</div>
				<div class="item item4">
					<div class="container">
						<div class="carousel-caption">

							<!-- <h3>Make good things happen</h3>

							<div class="bnr-button">
								<a class="act" href="single.html">Read More</a>
							</div> -->
						</div>
					</div>
				</div>
			</div>
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="fa fa-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				<span class="fa fa-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
			<!-- The Modal -->
		</div>
	</div>
	<!-- <?php if ($this->session->flashdata('message')) :
	    echo $this->session->flashdata('message');
	  endif; ?>
	<div class="banner_bottom donate-log">
		<div class="donate-inner">

			<div class="col-md-4 donate-f-login">
				<div class="donate-log-in book-form">
					<form action="<?= base_url('sampah/nasabah'); ?>" method="post">
						<input type="text" name="nama_lengkap" placeholder="Nama Lengkap Nasabah" required="" />
						<select name="jenis_sampah" class="form-control select2">
							<option value="Organik">Organik</option>
							<option value="Anorganik">Anorganik</option>
						</select><br>
						<input type="text" name="alamat" placeholder="Alamat" required="" />
						<input type="text" name="no_hp" placeholder="No. Handphone" required="" />
						<input type="text" name="nama" placeholder="Nama Sampah" required="" />
						<input type="number" name="harga" placeholder="Harga" required="" />
						<input type="number" name="jumlah" placeholder="Jumlah" required="" />
						<input type="text" name="satuan" placeholder="Satuan" required="" />
						<input type="submit" value="Submit">
					</form>
				</div>
			</div>
			<div class="col-md-8 donate-info">
				<h4>Submit Data Sampah dari Nasabah</h4>
			</div>
			<div class="clearfix"></div>
		</div>
	</div> -->
	<div class="banner_bottom" id="about">
		<div class="container">
			<h3 class="tittle_w3_agileinfo">Dokumentasi</h3>
			<div class="inner_sec_info_w3layouts">
				<div class="help_full">
					<ul class="rslides" id="slider4">
						<li>
							<div class="respon_info_img">
								<img src="<?= base_url(); ?>assets/website/images/foto/3.jpg" class="img-responsive" alt="Relief">
							</div>
							<div class="banner_bottom_left">
								<!-- <h4>Feed For Hungry Child</h4>

								<p>Maecenas quis neque libero. Class aptent taciti.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Pellentesque
									convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar
									neque pharetra ac.</p>
								<p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget
									pulvinar neque pharetra ac.</p>
								<div class="ab_button">
									<a class="btn btn-primary btn-lg hvr-underline-from-left" href="single.html" role="button">Read More </a>
								</div> -->
							</div>
						</li>
						<li>
							<div class="respon_info_img">
								<img src="<?= base_url(); ?>assets/website/images/foto/2.jpg" class="img-responsive" alt="Relief">
							</div>
							<div class="banner_bottom_left">
								<!-- <h4>Education to Every Child</h4>

								<p>Maecenas quis neque libero. Class aptent taciti.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Pellentesque
									convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar
									neque pharetra ac.</p>
								<p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget
									pulvinar neque pharetra ac.</p>
								<div class="ab_button">
									<a class="btn btn-primary btn-lg hvr-underline-from-left" href="single.html" role="button">Read More </a>
								</div> -->
							</div>
						</li>
						<li>
							<div class="respon_info_img">
								<img src="<?= base_url(); ?>assets/website/images/foto/1.jpg" class="img-responsive" alt="Relief">
							</div>
							<div class="banner_bottom_left">
								<!-- <h4>Patient and Family Support</h4>
								<p>Maecenas quis neque libero. Class aptent taciti.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Pellentesque
									convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar
									neque pharetra ac.</p>
								<p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget
									pulvinar neque pharetra ac.</p>
								<div class="ab_button">
									<a class="btn btn-primary btn-lg hvr-underline-from-left" href="single.html" role="button">Read More </a>
								</div> -->
							</div>
						</li>
						<li>
							<div class="respon_info_img">
								<img src="<?= base_url(); ?>assets/website/images/foto/4.jpg" class="img-responsive" alt="Relief">
							</div>
							<div class="banner_bottom_left">
								<!-- <h4>Provide Treatment</h4>
								<p>Maecenas quis neque libero. Class aptent taciti.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Pellentesque
									convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar
									neque pharetra ac.</p>
								<p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget
									pulvinar neque pharetra ac.</p>
								<div class="ab_button">
									<a class="btn btn-primary btn-lg hvr-underline-from-left" href="single.html" role="button">Read More </a>
								</div> -->
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="footer" id="contact">
		<div class="footer_inner_info_wthree_agileits">
			<!--/tabs-->
			<div class="responsive_tabs">
				<div id="horizontalTab">
					<ul class="resp-tabs-list">
						<li>Contact Info</li>
						<li>View Map</li>
					</ul>
					<div class="resp-tabs-container">
						<div class="tab1">
							<div class="tab-info">

								<div class="address">
									<div class="col-md-12 address-grid">
										<div class="address-left">
											<div class="dodecagon f1">
												<div class="dodecagon-in f1">
													<div class="dodecagon-bg f1">
														<span class="fas fa-map-marker-alt" aria-hidden="true"></span>
													</div>
												</div>
											</div>
										</div>
										<div class="address-right">
											<h6>Alamat</h6>
											<p>Jl. Raya Kucen No.78, Karangrejo, Kec. Karangrejo, Kabupaten Tulungagung, Jawa Timur 66253</p>
										</div>
										<div class="clearfix"> </div>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
						</div>
						<div class="tab3">

							<div class="tab-info">
								<div class="contact-map">

									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.974363980683!2d111.9094849!3d-8.001580500000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e78fd2cdfd21303%3A0x8242bcfda9ad5fd1!2sBengkel%20Las%20Bubut%20Aneka%20Karya!5e0!3m2!1sid!2sid!4v1658907248827!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
								</div>

							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<!--//tabs-->
			<div class="clearfix"> </div>
			<p class="copy-right"><?= date('Y') ?> © Bank Sampah Mekar Jaya. All rights reserved.</p>
		</div>
	</div>
	<!-- //footer -->
	<script type="text/javascript" src="<?= base_url(); ?>assets/website/js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="<?= base_url(); ?>assets/website/js/bootstrap.js"></script>
	<!-- script for responsive tabs -->
	<script src="<?= base_url(); ?>assets/website/js/easy-responsive-tabs.js"></script>
	<script>
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion           
				width: 'auto', //auto or any width like 600px
				fit: true, // 100% fit in a container
				closed: 'accordion', // Start closed if in accordion view
				activate: function (event) { // Callback function if tab is switched
					var $tab = $(this);
					var $info = $('#tabInfo');
					var $name = $('span', $info);
					$name.text($tab.text());
					$info.show();
				}
			});
			$('#verticalTab').easyResponsiveTabs({
				type: 'vertical',
				width: 'auto',
				fit: true
			});
		});
	</script>
	<!--// script for responsive tabs -->
	<script src="<?= base_url(); ?>assets/website/js/responsiveslides.min.js"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function () {
			// Slideshow 4
			$("#slider4").responsiveSlides({
				auto: true,
				pager: true,
				nav: false,
				speed: 500,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});

		});
	</script>
	<script type="text/javascript" src="<?= base_url(); ?>assets/website/js/all.js"></script>
	<script>
		$('ul.dropdown-menu li').hover(function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
	</script>

	<!-- js -->
	<script type="text/javascript" src="<?= base_url(); ?>assets/website/js/simplyCountdown.js"></script>
	<link href="<?= base_url(); ?>assets/website/css/simplyCountdown.css" rel='stylesheet' type='text/css' />
	<script>
		var d = new Date();
		simplyCountdown('simply-countdown', {
			year: d.getFullYear(),
			month: d.getMonth() + 2,
			day: 25
		});
		simplyCountdown('simply-countdown-custom', {
			year: d.getFullYear(),
			month: d.getMonth() + 2,
			day: 25
		});
		$('#simply-countdown-losange').simplyCountdown({
			year: d.getFullYear(),
			month: d.getMonth() + 2,
			day: 25
		});
	</script>
	<!--js-->
	<!--/tooltip -->
	<script>
		$(function () {
			$('[data-toggle="tooltip"]').tooltip({
				trigger: 'manual'
			}).tooltip('show');
		});

		// $( window ).scroll(function() {   
		// if($( window ).scrollTop() > 10){  // scroll down abit and get the action   
		$(".progress-bar").each(function () {
			each_bar_width = $(this).attr('aria-valuenow');
			$(this).width(each_bar_width + '%');
		});

		//  }  
		// });
	</script>
	<!--//tooltip -->
	<!-- Smooth-Scrolling-JavaScript -->
	<script type="text/javascript" src="<?= base_url(); ?>assets/website/js/easing.js"></script>
	<script type="text/javascript" src="<?= base_url(); ?>assets/website/js/move-top.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll, .navbar li a, .footer li a").click(function (event) {
				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- //Smooth-Scrolling-JavaScript -->
	<script type="text/javascript">
		$(document).ready(function () {
			/*
									var defaults = {
							  			containerID: 'toTop', // fading element id
										containerHoverID: 'toTopHover', // fading element hover id
										scrollSpeed: 1200,
										easingType: 'linear' 
							 		};
									*/

			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>


	<a href="#home" class="scroll" id="toTop" style="display: block;">
		<span id="toTopHover" style="opacity: 1;"> </span>
	</a>

	<!-- jQuery-Photo-filter-lightbox-Gallery-plugin -->
	<script type="text/javascript" src="<?= base_url(); ?>assets/website/js/jquery-1.7.2.js"></script>
	<script src="<?= base_url(); ?>assets/website/js/jquery.quicksand.js" type="text/javascript"></script>
	<script src="<?= base_url(); ?>assets/website/js/script.js" type="text/javascript"></script>
	<script src="<?= base_url(); ?>assets/website/js/jquery.prettyPhoto.js" type="text/javascript"></script>
	<!-- //jQuery-Photo-filter-lightbox-Gallery-plugin -->


</body>

</html>