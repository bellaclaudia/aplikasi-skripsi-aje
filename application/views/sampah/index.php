<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Sampah</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Sampah
        </div>
        <div style="overflow-x:auto;">
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <?php if ($this->session->userdata['level'] == '1') : ?>
          <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarian', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Kategori Sampah</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_jenis_sampah">
                  <option value="0" selected disabled>Pilih Jenis Sampah</option>
                  <?php foreach ($jenis_sampah as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['jenis_sampah'] ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_jenis_sampah') ?>
                </small>
              </div>
            </div>
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>&nbsp;
            </form>
            <hr>
            
          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br><?php endif ?>
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th></th>
                <?php if ($this->session->userdata['level'] == '1') : ?>
                <th>Nama Nasabah</th>
                <th>Alamat</th>
                <th>No. Handphone</th>
                <?php endif ?>
                <th>Jenis Sampah</th>
                <th>Harga</th>
                <th>Jumlah</th>
                <th>Total</th>
                <th>Satuan</th>
                <?php if ($this->session->userdata['level'] == '1') : ?>
                <th>Tgl. Transaksi</th>
                <th>Aksi</th>
                <?php endif ?>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($data_sampah as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <?php if ($this->session->userdata['level'] == '1') : ?>
                  <td><?= $row->nama_nasabah ?></td>
                  <td><?= $row->alamat ?></td>
                  <td><?= $row->no_hp ?></td>
                  <?php endif ?>
                  <td><?= $row->jenis_sampah ?></td>
                  <td><?= $row->harga_nasabah ?></td>
                  <td><?= $row->jumlah ?></td>
                  <td><?= $row->jumlah*$row->harga_nasabah ?></td>
                  <td><?= $row->satuan ?></td>
                  <?php if ($this->session->userdata['level'] == '1') : ?>
                  <td><?= date('d-m-Y', strtotime($row->tgl_input)) ?></td>
                  <td>
                    <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('sampah/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Sampah <?= $row->jenis_sampah; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  </td>
                  <?php endif ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAdd', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Nama Nasabah</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-1" name="id_nasabah">
                        <option value="0" selected disabled>Pilih Nama Nasabah</option>
                        <?php foreach ($nasabah as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama'] ?></option>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger">
                        <?php echo form_error('id_nasabah') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Jenis Sampah</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-1" name="id_jenis_sampah">
                        <option value="0" selected disabled>Pilih Jenis Sampah</option>
                        <?php foreach ($jenis_sampah as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['jenis_sampah'] ?></option>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger">
                        <?php echo form_error('id_jenis_sampah') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Harga Nasabah</label>
                    <div class="col-md-9">
                      <input class="form-control" type="number" placeholder="Isi Harga Nasabah" name="harga_nasabah" required>
                      <small class="text-danger">
                        <?php echo form_error('harga_nasabah') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Jumlah</label>
                    <div class="col-md-9">
                      <input class="form-control" type="number" placeholder="Isi Jumlah" name="jumlah" required>
                      <small class="text-danger">
                        <?php echo form_error('jumlah') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Satuan</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" placeholder="Isi Satuan" name="satuan" vrequired>
                      <small class="text-danger">
                        <?php echo form_error('satuan') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_sampah as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo site_url('sampah/edit'); ?>" method="post">
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nama Nasabah</label>
                        <div class="col-md-9">
                           <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
                          <select class="form-control select2-single" id="select2-2" name="id_nasabah">
                            <option value="0" selected disabled>Pilih Nama Nasabah</option>
                            <?php foreach ($nasabah as $k) : ?>
                              <option value=" <?= $k['id']; ?>" <?php if ($k['id'] == $row->id_nasabah) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                            <?php endforeach; ?>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('id_nasabah') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Jenis Sampah</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-1" name="id_jenis_sampah">
                            <option value="0" selected disabled>Pilih Jenis Sampah</option>
                            <option value="0" selected disabled>Pilih Nama Nasabah</option>
                            <?php foreach ($jenis_sampah as $k) : ?>
                              <option value=" <?= $k['id']; ?>" <?php if ($k['id'] == $row->id_jenis_sampah) { ?>selected<?php } ?>><?= $k['jenis_sampah'] ?></option>
                            <?php endforeach; ?>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('id_jenis_sampah') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Harga Nasabah</label>
                        <div class="col-md-9">
                          <input class="form-control" type="number" placeholder="Isi Harga Nasabah" name="harga_nasabah" value="<?= $row->harga_nasabah; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('harga_nasabah') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Jumlah</label>
                        <div class="col-md-9">
                          <input class="form-control" type="number" placeholder="Isi Jumlah" name="jumlah" value="<?= $row->jumlah; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('jumlah') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Satuan</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" placeholder="Isi Satuan" name="satuan" value="<?= $row->satuan; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('satuan') ?>
                          </small>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>