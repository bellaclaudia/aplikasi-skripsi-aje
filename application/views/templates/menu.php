<div class="app-body">
  <div class="sidebar">
    <nav class="sidebar-nav">
      <ul class="nav">
        <li class="nav-title">Menu</li>
        <?php if (isset($this->session->userdata['logged_in'])) { // jika udh lgin 
        ?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard'); ?>">
              <i class="nav-icon icon-speedometer"></i> Dashboard
            </a>
          </li>
          <?php if ($this->session->userdata['level'] == '1') { // administrator 
          ?>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('nasabah'); ?>">
                <i class="nav-icon icon-list"></i> Data Nasabah
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('transaksi'); ?>">
                <i class="nav-icon icon-list"></i> Transaksi Penjualan
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('sampah'); ?>">
                <i class="nav-icon icon-list"></i> Data Sampah
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('jenis_sampah'); ?>">
                <i class="nav-icon icon-list"></i> Jenis Sampah
              </a>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-share-alt"></i> Pengaturan</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_user'); ?>">
                    <i class="nav-icon fa fa-user"></i> Data User
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('ganti_pass') ?>">
                    <i class="nav-icon fa fa-users"></i> Ganti Password
                  </a>
                </li>
              </ul>
            </li>
          <?php } else if ($this->session->userdata['level'] == '2') {
          ?>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('sampah'); ?>">
                <i class="nav-icon icon-list"></i> Data Sampah
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('transaksi'); ?>">
                <i class="nav-icon icon-list"></i> Transaksi Pembelian
              </a>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-share-alt"></i> Pengaturan</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('ganti_pass') ?>">
                    <i class="nav-icon fa fa-users"></i> Ganti Password
                  </a>
                </li>
              </ul>
            </li>
        <?php }
        }
        ?>
      </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
  </div>