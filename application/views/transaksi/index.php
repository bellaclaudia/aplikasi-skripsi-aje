<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Transaksi</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Transaksi
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <?php if ($this->session->userdata['level'] == '1') : ?>
          <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarian', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Sampah</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_jenis_sampah">
                  <option value="0" selected disabled>Pilih Jenis Sampah</option>
                  <?php foreach ($jenis_sampah as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['jenis_sampah'] ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_jenis_sampah') ?>
                </small>
              </div>
            </div>
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>&nbsp;
            </form>
            <hr>
            <?php endif ?>
            <?php if ($this->session->userdata['level'] == '1') : ?>
            <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br><?php endif ?>
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th></th>
                <th>Nama Pengepul</th>
                <th>Alamat</th>
                <th>No. Handphone</th>
                <th>Tanggal</th>
                <th>Jenis Sampah</th>
                <th>Jumlah</th>
                <th>Harga Jual</th>
                <th>Total</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($data_transaksi as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $row->nama_nasabah ?></td>
                  <td><?= $row->alamat ?></td>
                  <td><?= $row->no_hp ?></td>
                  <td><?= date('d-m-Y', strtotime($row->tgl_input)) ?></td>
                  <td><?= $row->jenis_sampah ?></td>
                  <td><?= $row->jumlah ?></td>
                  <td><?= $row->harga_jual ?></td>
                  <td><?= $row->total_harga ?></td>
                  <td>
                    <a href="<?php echo site_url('transaksi/invoice/' . $row->id); ?>" class="btn btn-primary btn-circle" title="Invoice"><i class="fa fa-print"></i></a>
                    <?php if ($this->session->userdata['level'] == '1') : ?>
                    <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('transaksi/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Transaksi dengan ID <?= $row->id; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                    <?php endif ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAdd', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Nama Pengepul</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-1" name="id_user">
                        <option value="0" selected disabled>Pilih Nama Pengepul</option>
                        <?php foreach ($user as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama'] ?></option>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger">
                        <?php echo form_error('id_user') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Data Sampah</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-2" name="id_sampah">
                        <option value="0" selected disabled>Pilih Data Sampah</option>
                        <?php foreach ($sampah as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['jenis_sampah'] . " (Jumlah Tersedia " . $k['jumlah'] . " " . $k['satuan'] . " | Harga " . $k['harga_nasabah'] . " Per " . $k['satuan'] . ")"; ?></option>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger">
                        <?php echo form_error('id_sampah') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Harga Jual</label>
                    <div class="col-md-9">
                      <input class="form-control" type="number" name="harga_jual" placeholder="Isi Harga Jual" required>
                      <small class="text-danger">
                        <?php echo form_error('harga_jual') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Jumlah</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="jumlah" placeholder="Isi Jumlah" required>
                      <small class="text-danger">
                        <?php echo form_error('jumlah') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_transaksi as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo site_url('transaksi/edit'); ?>" method="post">
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nama Pengepul</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-3" name="id_user">
                            <option value="0" selected disabled>Pilih Nama Pengepul</option>
                            <?php foreach ($user as $k) : ?>
                              <option value=" <?= $k['id']; ?>" <?php if ($k['id'] == $row->id_user) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                            <?php endforeach; ?>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('id_user') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Data Sampah</label>
                        <div class="col-md-9">
                          <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
                          <select class="form-control select2-single" id="select2-4" name="id_sampah">
                            <option value="0" disabled>Pilih Data Sampah</option>
                            <?php foreach ($sampah as $k) : ?>
                              <option value=" <?= $k['id']; ?>" <?php if ($k['id'] == $row->id_sampah) { ?>selected<?php } ?>><?= $k['jenis_sampah'] . " (Jumlah Tersedia " . $k['jumlah'] . " " . $k['satuan'] . " | Harga " . $k['harga_nasabah'] . " Per " . $k['satuan'] . ")"; ?></option>
                            <?php endforeach; ?>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('id_sampah') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                    <label class="col-md-3 col-form-label">Harga Jual</label>
                    <div class="col-md-9">
                      <input class="form-control" type="number" name="harga_jual" placeholder="Isi Harga Jual" value="<?= $row->harga_jual; ?>" required>
                      <small class="text-danger">
                        <?php echo form_error('harga_jual') ?>
                      </small>
                    </div>
                  </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Jumlah</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="jumlah" placeholder="Isi Jumlah" value="<?= $row->jumlah; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('jumlah') ?>
                          </small>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>