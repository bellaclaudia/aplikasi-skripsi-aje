<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=transaksi.xls");
header("Cache-control: public");
?>
<style>
    .allcen{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .allcen2{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .str{ 
        mso-number-format:\@; 
    }
</style>
<head>
    <meta charset="utf-8" />
    <title>Data Transaksi</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <style>
        body {
            font-family: verdana,arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            font-weight: 400;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
        }
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size:11px;
            width: 100%;
            color:#333333;
            border-width: 1px;
            border-color: #e9e9e9;
            border-collapse: collapse;
        }
        table.gridtable th {
            border-width: 1px;
            padding: 8px;
            font-size:12px;
            border-style: solid;
            font-weight: 900;
            color: #ffffff;
            border-color: #e9e9e9;
            background: #ea6153;
        }
        table.gridtable td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #e9e9e9;
            background-color: #ffffff;
        }

    </style>
</head>
<body>
<table width="100%" class="gridtable">
	<tr>
		<td colspan="5">
			<h4 style="text-align: center">Bank Sampah Mekar Jaya<br>Data Transaksi</h4>
		</td>
	</tr>
</table>
<table border="1" width="100%" class="gridtable">
  <thead style="background-color: blue">
    <tr>
      	<th class="allcen center bold">No.</th>
      	<th class="allcen center bold">Nama Pengepul</th>
        <th class="allcen center bold">Alamat</th>
        <th class="allcen center bold">No. Handphone</th>
        <th class="allcen center bold">Tanggal</th>
        <th class="allcen center bold">Jenis Sampah</th>
        <th class="allcen center bold">Jumlah</th>
        <th class="allcen center bold">Harga Jual</th>
        <th class="allcen center bold">Total</th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($data_transaksi as $row) : ?>
      <tr>
        <td><?= $i++; ?></td>
        <td><?= $row->nama_nasabah ?></td>
          <td><?= $row->alamat ?></td>
          <td><?= $row->no_hp ?></td>
          <td><?= date('d-m-Y', strtotime($row->tgl_input)) ?></td>
          <td><?= $row->jenis_sampah ?></td>
          <td><?= $row->jumlah ?></td>
          <td><?= $row->harga_jual ?></td>
          <td><?= $row->total_harga ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>