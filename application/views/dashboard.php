<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <?php if ($this->session->userdata['level'] == 1) { ?>
      <li class="breadcrumb-item">
        <a href="<?= base_url('dashboard'); ?>">Admin</a>
      </li>
    <?php } ?>
    <li class="breadcrumb-item active">Dashboard</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12 col-lg-12">
          <div class="card-body">
            <div class="alert alert-primary" role="alert"><i class="nav-icon icon-speedometer"></i> Dashboard</div>
            <?php //if ($this->session->userdata['level'] == '1') {
            $this->db->where("level", $this->session->userdata['level']);
            $cek = $this->db->get('master_user');

            if ($cek->num_rows() > 0) {
              foreach ($cek->result() as $ck) {
                if($ck->level == 1) {
                  $nama_grup = "Admin";
                }elseif($ck->level == 2) {
                  $nama_grup = "Pengepul";
                }
              }
            }

            ?>

            <div class="alert alert-primary" role="alert">Selamat Datang di Aplikasi Bank Sampah Mekar Jaya, Anda login sebagai <?= $nama_grup ?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>